<?php include 'inc/header.php';?>
<?php 
    include 'lib/config.php';
    include 'lib/Database.php';
    $db = new Database();
?>
 <div class="myform">
     
     <?php 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $permited      = array('jpg', 'jpeg', 'png', 'gif');
            $filename      = $_FILES['image']['name'];
            $filesize      = $_FILES['image']['size'];
            $file_tmp      = $_FILES['image']['tmp_name'];

            //Generate unique image name here
            $divided       = explode('.', $filename);
            $file_ext      = strtolower(end($divided)); // separate file extantion here. ex. jpg, png
            $gen_filename  = substr(md5(time()), 0, 10).'.'.$file_ext;

            //Image validation here
            if(empty($filename)){
                echo "<span class='error'>Please select any image!</span>";
            }elseif($filesize > 1048576){
                echo "<span class='error'>Image Size Should Be Less Than  1 MB!</span>";
            }elseif(in_array($file_ext, $permited) === FALSE){
                echo "<span class='error'>You can upload only:-".implode(', ', $permited)."</span>";
            }else{
                // file upload to directory
                $fileupload    = "uploads/".$gen_filename;
                move_uploaded_file($file_tmp, $fileupload);

                //file upload to database
                $query  = "INSERT INTO tbl_fileuplad (img)VALUES('$fileupload')";
                $insert = $db->inserImg($query);
                if($insert){
                    echo "<span class='success'>Image Inserted successfully!</span>";
                }else{
                    echo "<span class='error'>Image Not Inserted!</span>";
                }
            }
        }
     ?>
     
  <form action="" method="post" enctype="multipart/form-data">
   <table>
    <tr>
     <td>Select Image</td>
     <td><input type="file" name="image"/></td>
    </tr>
    <tr>
     <td></td>
     <td><input type="submit" name="submit" value="Upload"/></td>
    </tr>
   </table>
  </form>
  <?php 
    echo "<h2>Last uploaded image:</h2>";

    $query = "SELECT * FROM tbl_fileuplad ORDER BY id DESC LIMIT 1";
    $getImage = $db->selectImage($query);
    if($getImage){
        while ( $row = $getImage->fetch_assoc()) {
            echo "<img src='".$row['img']."' alt='image' height='150' width='150'>";
        }
    }
  ?>
  <hr>
  <div><a href="allimage.php">View All Image >> </a></div>
 </div>
<?php include 'inc/footer.php';?>