<?php 

	/**
	* Database class
	*/
	class Database
	{
		public $host   = DB_HOST;
		public $user   = DB_USER;
		public $pass   = DB_PASS;
		public $dbname = DB_NAME;

		public $error;
		public $link;
		
		function __construct()
		{
			$this->connectDB();
		}

		// Database connect

		private function connectDB(){
			$this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);

			if( !$this->link){
				$this->error = "Connection faild".$this->link->connect_error;
				return false;
			}
		}

		// Insert data

		public function inserImg($data){
			$insert = $this->link->query($data) or die($this->link->error.__LINK__);
			
			if($insert){
				return $insert;
			}else{
				return false;
			}
		}

		// Select data
		public function selectImage($data){
			$results = $this->link->query($data)  or die($this->link->error.__LINK__);
			if($results->num_rows > 0){
				return $results;
			}else{
				return false;
			}
		}

		// Delete data
		public function deleteImage($data){
			$deleteImg = $this->link->query($data) or die($this->link->error.__LINK__);
			if($deleteImg){
				return $deleteImg;
			}else{
				return false;
			}
		}
	}

?>