<?php include 'inc/header.php';?>
<?php 
    include 'lib/config.php';
    include 'lib/Database.php';
    $db = new Database();
?>
<style type="text/css">
    img{max-width: 100%;height: auto;vertical-align: top;}
    .clear:after, .clear:before{
        display: block;
        content: "";
    }
    .clear:after{clear: both;}
    .gallery_wrrp{
        width:100%;
        position: relative;
    }
    .gallery_list{
        margin-left:-30px;
    }
    .single_image{
        float: left;
        margin-left: 30px;
        margin-bottom: 50px;
        width:27.5%;
    }
    .single_image img{
        width: 100%;
    }
    .single_image a{
        display: block;
        text-align: center;
    }
</style>
 <div class="myform">
    <?php 
        if(isset($_GET['del'])){
            $id = $_GET['del'];

            $query = "SELECT * FROM tbl_fileuplad WHERE id = '$id'";
            $getImg = $db->selectImage($query);
            if($getImg){
                while ( $row = $getImg->fetch_assoc()) {
                    unlink($row['img']);
                }
            }
            $query = "DELETE FROM tbl_fileuplad WHERE id ='$id'";
            $delete = $db->deleteImage($query); 
            if($delete){
                echo "<span class='success'>Image Deleted successfully!</span>";
            }else{
                echo "<span class='error'>Image Not Deleted!</span>";
            }
        }
    ?>

    <h2>Uploaded Image List:</h2>
    <div class="gallery_wrrp">
        <div class="gallery_list">
          <?php 
            
            $query = "SELECT * FROM tbl_fileuplad ORDER BY id DESC";
            $getImage = $db->selectImage($query);
            if($getImage){
                while ( $row = $getImage->fetch_assoc()) {
            ?>
            <div class="single_image">
                <img src='<?php echo $row['img']; ?>' alt='image'>
                <a href="?del=<?php echo $row['id']; ?>"> Delete</a>
            </div>
            <?php
                }
              }
            ?>
        </div>
    </div>
    <div class="clear"></div>
  <hr>
  <div><a href="index.php"> << Home Page</a></div>
 </div>
<?php include 'inc/footer.php';?>